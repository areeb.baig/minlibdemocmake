<!DOCTYPE html>
<html lang="en">
<body>

<h1>Minimal Library Demonstration</h1>

<h2>Project Description</h2>

<p>This project is a demonstration of a minimal C++ library and a main program that uses this library. The library contains basic mathematical functions, while the main program demonstrates how to use these functions.</p>

<h2>Directory Structure</h2>

<pre>
project/
│
├── math_library/           # Directory for the math library
│   ├── math_functions.h   # Header file for math functions library
│   ├── math_functions.cpp # Implementation file for math functions library
│   └── CMakeLists.txt     # CMake configuration for the math library
│
├── main_program/           # Directory for the main program
│   ├── main.cpp           # Source file for the main program
│   └── CMakeLists.txt     # CMake configuration for the main program
│
└── CMakeLists.txt          # Root CMake configuration for the entire project
</pre>

<h2>How to Build and Run</h2>

<ol>
  <li><strong>Clone the Repository</strong>: Clone this repository to your local machine using Git.</li>

  <code>git clone https://gitlab.com/areeb.baig/minlibdemocmake</code>

  <li><strong>Navigate to the Project Directory</strong>: Change your current directory to the root directory of the cloned repository.</li>

  <code>cd minlibdemocmake</code>

  <li><strong>Create a Build Directory</strong>: Create a directory to hold the build files. This directory should be separate from your source code directory.</li>

  <code>mkdir build</code>

  <li><strong>Navigate to Build Directory</strong>: Change your current directory to the newly created build directory.</li>

  <code>cd build</code>

  <li><strong>Generate Build Files</strong>: Run CMake to generate the build files based on the CMakeLists.txt configuration.</li>

  <code>cmake ..</code>

  <li><strong>Build the Project</strong>: Use your preferred build tool (e.g., make, ninja) to build the project.</li>

  <code>make</code>

  <li><strong>Run the Program</strong>: Once the build process completes successfully, you can find the executable file in the bin directory.</li>

  <code>./bin/main_program</code>

  <p>This will execute the main program, which demonstrates the usage of functions from the math library.</p>
</ol>

</body>
</html>
