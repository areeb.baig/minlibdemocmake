#ifndef MATH_LIBRARY_H
#define MATH_LIBRARY_H

class Math {
public:
    static int add(int a, int b);
    static int subtract(int a, int b);
    static int multiply(int a, int b);
    static float divide(float a, float b);
};

#endif  // MATH_LIBRARY_H
