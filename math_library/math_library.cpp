#include "math_library.h"

int Math::add(int a, int b) {
    return a + b;
}

int Math::subtract(int a, int b) {
    return a - b;
}

int Math::multiply(int a, int b) {
    return a * b;
}


float Math::divide(float a, float b) {
    if (b == 0) {
        return 0; // Division by zero is undefined, returning 0 as a placeholder
    }
    return a / b;
}
