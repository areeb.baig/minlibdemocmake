#include "math_library.h"
#include <iostream>

int main() {
    int a = 10, b = 5;

    std::cout << "Enter Number 01: ";
    std::cin >> a;
    std::cout << "Enter Number 02: ";
    std::cin >> b;
    std::cout << "\n]=> Performing Mathematical Operations [+]\n\n";
    std::cout << "[+] Addition: " << Math::add(a, b) << std::endl;
    std::cout << "[-] Subtraction: " << Math::subtract(a, b) << std::endl;
    std::cout << "[x] Multiplication: " << Math::multiply(a, b) << std::endl;
    std::cout << "[/] Division: " << Math::divide(a, b) << std::endl;

    return 0;
}
